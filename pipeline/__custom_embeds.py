from typing import Any, Callable, Dict, Iterable, Tuple, Union, List

from .__embed import AbstractEmbed


class TupleEmbed(AbstractEmbed):
    """
    For functions of the form:
    func(arg[Tuple]) -> out[Tuple]

    E.g.

    dot_prod = Embed(sum) >> Embed(prod)

    # equivalent to...

    def dot_prod(vectors):
        res1 = prod(vectors)
        res2 = sum(res1)

        return res2
    """

    @staticmethod
    def __baseclass_concat__(a: Tuple[Any], b: Tuple[Any]) -> Tuple[Any]:
        return a + b

    @staticmethod
    def compose(
        callable_external: Callable[[Tuple[Any]], Tuple[Any]],
        callable_internal: Callable[[Tuple[Any]], Tuple[Any]],
        kwargs_external: Union[Dict[str, Any], None],
        kwargs_internal: Union[Dict[str, Any], None]
    ):
        kwargs_external = {} if kwargs_external is None else kwargs_external
        kwargs_internal = {} if kwargs_internal is None else kwargs_internal

        def new_func(args: Tuple[Any]) -> Tuple[Any]:
            return callable_external(callable_internal(args, **kwargs_internal))

        return TupleEmbed(new_func, kwargs=kwargs_external)


class Composable(AbstractEmbed):
    @staticmethod
    def compose(
        callable_external: Callable[[Any], Any],
        callable_internal: Callable[[Any], Any],
        kwargs_external: Union[Dict[str, Any], None],
        kwargs_internal: Union[Dict[str, Any], None]
    ):
        kwargs_external = {} if kwargs_external is None else kwargs_external
        kwargs_internal = {} if kwargs_internal is None else kwargs_internal

        def new_func(args: Any) -> Any:
            return callable_external(callable_internal(args, **kwargs_internal))

        return Composable(new_func, kwargs=kwargs_external)

    @staticmethod
    def vectorize(
        callable: Callable[[Any], Any],
        kwargs: Dict[str, Any]
    ) -> 'AbstractEmbed':
        def new_func(arg_iter: Iterable[Any], **kwargs) -> List[Any]:
            res = []
            for arg in arg_iter:
                res.append(callable(arg, **kwargs))

            return res

        return Composable(new_func, kwargs=kwargs)


try:
    from numpy import concatenate as np_concatenate, ndarray

    class NumpyEmbed(AbstractEmbed):
        """
        For functions of the form
        func(np.ndarray) -> np.ndarray
        """

        @staticmethod
        def __baseclass_concat__(a: Union[ndarray, Iterable[Any]], b: Union[ndarray, Iterable[Any]]) -> ndarray:
            return np_concatenate([a, b], axis=0)

        @staticmethod
        def compose(
            callable_external: Callable[[ndarray], ndarray],
            callable_internal: Callable[[ndarray], Any],
            kwargs_external: Union[Dict[str, Any], None],
            kwargs_internal: Union[Dict[str, Any], None]
        ):
            kwargs_external = {} if kwargs_external is None else kwargs_external
            kwargs_internal = {} if kwargs_internal is None else kwargs_internal

            def new_func(array: ndarray):
                return callable_external(callable_internal(array))

            return NumpyEmbed(new_func)
except ImportError:
    pass

# def numpy_embed(func):
#     """
#     Python decorator to embed a numpy function

#     E.g.
#     @numpy_embed
#     def identity(x):
#         return x

#     print((identity * identity)(9))
#     # 81
#     """
#     return NumpyEmbed(func)

# def tuple_embed(func):
#     """
#     Python decorator to embed a tuple function

#     E.g.
#     @tuple_embed
#     def identity(x):
#         return x

#     print((identity + identity)((9, 7)))
#     # (9, 7, 9, 7)
#     """
#     return TupleEmbed(func)
