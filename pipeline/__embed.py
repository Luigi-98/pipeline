from abc import abstractmethod
from typing import Any, Callable, Dict, Union, overload, Literal


EllipsisTyping = Literal[...]  # type: ignore


class AbstractEmbed:
    """
    Embeds a function so that it can be concatenated or composed

    This is an abstract class,
    the following methods must be implemented for a class of type T:

    @staticmethod
    __baseclass_concat__(a: T, b: T) -> T

        > this method will be called for output concatenation
        > e.g. for tuples,
        >   __baseclass_concat__((2, 3), (4,)) == (2, 3, 4)

    compose(self: MyEmbed, other: MyEmbed) -> MyEmbed

        > this method implements lambda x: self(other(x))
        > the output embedding must have
        >   the same kwargs as self.kwargs
    """
    def __init__(
        self,
        func: Callable[..., Any],
        kwargs: Union[Dict[str, Any], None] = None
    ):
        self.func = func
        self.kwargs = {} if kwargs is None else kwargs

    def copy(self) -> 'AbstractEmbed':
        return self.__new_instance__(self.func, **self.kwargs)

    def __call__(self, *args: Any, **kwargs: Any) -> Any:
        kwargs_now = self.kwargs.copy()
        kwargs_now.update(kwargs)

        res = self.func(*args, **kwargs_now)
        return res

    def __or__(self, other: Union[Callable[..., Any], EllipsisTyping]) -> 'AbstractEmbed':
        if other is Ellipsis:
            return self.__vectorize__(self)
        return self.concat(other)

    def __rshift__(self, other: Callable[..., Any]) -> 'AbstractEmbed':
        return self.__compose__(self, other)

    def __rrshift__(self, other: Callable[..., Any]) -> 'AbstractEmbed':
        return self.__compose__(other, self)

    @staticmethod
    @abstractmethod
    def compose(
        callable_external: Callable[..., Any],
        callable_internal: Callable[..., Any],
        kwargs_external: Union[Dict[str, Any], None],
        kwargs_internal: Union[Dict[str, Any], None]
    ) -> 'AbstractEmbed':
        """
        Abstract method.

        Implements function composition:
            callable_external(callable_internal(..., **kwargs_internal), **kwargs_external)

        When called from other internal functions,
            the callables won't be embeddings, but raw functions.

        Parameters
        ----------
        callable_external : Callable[..., Any]
            The outer function to compose.
        callable_internal : Callable[..., Any]
            The inner function to compose.
        kwargs_external : Dict[str, Any] or None
            Static kwargs for the external function
        kwargs_internal : Dict[str, Any] or None
            Static kwargs for the internal function

        Returns
        -------
        out : AbstractEmbed
            An embedded function that acts as the requested composition
            with no dynamic kwargs.
        """
        ...

    @staticmethod
    @abstractmethod
    def __baseclass_concat__(res1: Any, res2: Any) -> Any:
        raise NotImplementedError

    def __compose__(
        self,
        internal: Callable[..., Any],
        external: Callable[..., Any]
    ) -> 'AbstractEmbed':
        callable_external = external
        callable_internal = internal
        kwargs_external = {}
        kwargs_internal = {}
        if isinstance(external, AbstractEmbed):
            callable_external = external.func
            kwargs_external = external.kwargs
        if isinstance(internal, AbstractEmbed):
            callable_internal = internal.func
            kwargs_internal = internal.kwargs

        return self.compose(callable_external, callable_internal, kwargs_external, kwargs_internal)

    @staticmethod
    @abstractmethod
    def vectorize(
        callable: Callable[[Any], Any],
        kwargs: Dict[str, Any]
    ) -> 'AbstractEmbed':
        """
        Abstract method.

        Implements function vectorization. Applies the same function
            to all the values of the input iterable and returning an
            Iterable with all the resulting values.

        When called from other internal functions,
            the callable won't be an embedding, but a raw function.

        Parameters
        ----------
        callable : Callable[[Any], Any]
            The callable to iter on the values.
        kwargs : Dict[str, Any]
            kwargs that will be passed to each call of callable.

        Returns
        -------
        out : AbstractEmbed[Iterable[Any], Iterable[Any]]
            An embedded function that acts on Iterables elementwise.
        """
        ...

    def __vectorize__(self, callable: Callable[[Any], Any]) -> 'AbstractEmbed':
        kwargs = {}
        func = callable
        if isinstance(callable, AbstractEmbed):
            kwargs = callable.kwargs
            func = callable.func

        return self.vectorize(func, kwargs=kwargs)

    def __mul__(self, other: Any):
        return self.__new_instance__(self.__multiply__(self, other))

    def __rmul__(self, other: Any):
        return self.__new_instance__(self.__multiply__(other, self))

    def __add__(self, other: Any):
        return self.__new_instance__(self.__addition__(self, other))

    def __radd__(self, other: Any):
        return self.__new_instance__(self.__addition__(other, self))

    def concat(
        self,
        other: 'AbstractEmbed'
    ) -> 'AbstractEmbed':
        def new_func(args: Any):
            res1 = self.func(args, **self.kwargs)
            res2 = other.func(args, **other.kwargs)

            return self.__baseclass_concat__(res1, res2)
        return self.__new_instance__(new_func, kwargs={})

    @staticmethod
    @overload
    def __multiply__(a: Callable[..., Any], b: Any) -> Callable[..., Any]:
        ...

    @staticmethod
    @overload
    def __multiply__(a: Any, b: Callable[..., Any]) -> Callable[..., Any]:
        ...

    @staticmethod
    @overload
    def __addition__(a: Callable[..., Any], b: Any) -> Callable[..., Any]:
        ...

    @staticmethod
    @overload
    def __addition__(a: Any, b: Callable[..., Any]) -> Callable[..., Any]:
        ...

    @staticmethod
    def __multiply__(a: ..., b: ...):
        a_func: Callable[..., Any] = a
        a_kwargs = {}
        if isinstance(a, AbstractEmbed):
            a_func = a.func
            a_kwargs = a.kwargs
        b_func: Callable[..., Any] = b
        b_kwargs = {}
        if isinstance(b, AbstractEmbed):
            b_func = b.func
            b_kwargs = b.kwargs

        def prod_C_C(*input: Any) -> Any:
            return a_func(*input, *a_kwargs) * b_func(*input, *b_kwargs)

        def prod_A_C(*input: Any) -> Any:
            return a * b_func(input, *b_kwargs)

        def prod_C_A(*input: Any) -> Any:
            return a_func(input, *a_kwargs) * b

        def prod_A_A(*input: Any) -> Any:
            return a * b

        if isinstance(b, Callable):
            if isinstance(a, Callable):
                prod = prod_C_C
            else:
                prod = prod_A_C
        else:
            if isinstance(a, Callable):
                prod = prod_C_A
            else:
                prod = prod_A_A

        return prod

    @staticmethod
    def __addition__(a: ..., b: ...):
        a_func: Callable[..., Any] = a
        a_kwargs = {}
        if isinstance(a, AbstractEmbed):
            a_func = a.func
            a_kwargs = a.kwargs
        b_func: Callable[..., Any] = b
        b_kwargs = {}
        if isinstance(b, AbstractEmbed):
            b_func = b.func
            b_kwargs = b.kwargs

        def add_C_C(*input: Any) -> Any:
            return a_func(*input, *a_kwargs) + b_func(*input, *b_kwargs)

        def add_A_C(*input: Any) -> Any:
            return a + b_func(input, *b_kwargs)

        def add_C_A(*input: Any) -> Any:
            return a_func(input, *a_kwargs) + b

        def add_A_A(*input: Any) -> Any:
            return a + b

        if isinstance(b, Callable):
            if isinstance(a, Callable):
                add = add_C_C
            else:
                add = add_A_C
        else:
            if isinstance(a, Callable):
                add = add_C_A
            else:
                add = add_A_A

        return add

    @classmethod
    def __new_instance__(cls, func: Callable[..., Any], **kwargs: Dict[str, Any]):
        return cls(func, **kwargs)
