class Pipeline():
    def __init__(self, functions=None):
        self.functions = [] if functions is None else functions

    def __call__(self, arg0):
        out = arg0
        for function in self.functions:
            out = function(out)
        return out

    def add(self, function):
        self.functions.append(function)

    def __str__(self):
        return "\n".join([
            str(function) for function in self.functions
        ])

    def __repr__(self):
        return self.__str__()

    def __getitem__(self, idx):
        if type(idx) is slice:
            return Pipeline(functions=self.functions[idx])
        return self.functions[idx]

    def __setitem__(self, idx, value):
        self.functions[idx] = value
