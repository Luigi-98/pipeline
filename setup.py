#!/usr/bin/env python

from distutils.core import setup

setup(
    name="Pipeline",
    version="1.1.3",
    description="Piping and concatenating tool for callables",
    author="Luigi Privitera",
    packages=['pipeline']
)
