import pytest  # noqa
from pipeline import Composable


class Test_Composable:
    _inited = False

    def __init(self):
        if not self._inited:
            self._inited = True

            @Composable
            def function1(arg):
                return arg + 1

            @Composable
            def function2(arg):
                return arg * 2

            self.function1 = function1
            self.function2 = function2

    def test_function_decorator_init(self):
        self.__init()

    def test_function_composition(self):
        self.__init()

        function1 = self.function1
        function2 = self.function2

        assert (function1 >> function2)(2) == 6
        assert (function2 >> function1)(2) == 5

    def test_function_product(self):
        self.__init()

        function1 = self.function1
        function2 = self.function2

        assert (function1 * function2)(2) == 12

    def test_function_sum(self):
        self.__init()

        function1 = self.function1
        function2 = self.function2

        assert (function1 + function2)(2) == 7

    def test_function_vectorize(self):
        self.__init()

        function1 = self.function1

        assert tuple((function1 | ...)([1, 2, 3, 4])) == (2, 3, 4, 5)

    def test_kwargs(self):
        pass

    def test_composed_kwargs(self):
        pass
